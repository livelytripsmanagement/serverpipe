'use strict'
const status = require('http-status')
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../config');

module.exports = (app, options) => {
  const {repo} = options
    app.get('/', (req, res, next) => {
       res.send("done");
    })

    app.post('/login', (req, res, next) => {
        repo.getUserInfo(req,res).then(user => {
            validatePassword(user,req.body.payload.password).then(result=>{
                if(result.validPassword)
                createToken(user).then(response=>{
                    res.status(status.OK).json(response)
                })
                else {
                    res.status(status.UNAUTHORIZED).json(response)
                }
            })
        }).catch(next)
    })

  app.post('/disableUser', (req, res, next) => {
    repo.updateUser(req,res).then(movies => {
      res.status(status.OK).json(movies)
    }).catch(next)
  })

    app.post('/register', (req, res, next) => {
        const notificationService = req.container.resolve('notificationService');
        repo.addUser(req,res).then(user => {
            const payload = Object.assign({}, {name:"vj"}, {user: {name: "vj", email: "raagavan.vijay@gmail.com"}})
            notificationService(payload);
            createToken(user).then(response=> {
                res.status(status.OK).json(response)
            });
        }).catch(next)
    })
    app.post('/updateProfile', (req, res, next) => {
        repo.getAllMovies().then(movies => {
            res.status(status.OK).json(movies)
        }).catch(next)
    })
}

const createToken=(user)=>
{
    return new Promise((resolve, reject) => {
        var token = jwt.sign({id: user._id}, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        resolve({auth: true, token: token});
    });
}

const validatePassword=(user,password)=>
{
    return new Promise((resolve, reject) => {
        var passwordIsValid = bcrypt.compareSync(password, user.password);
        if(passwordIsValid) {
            resolve({validPassword: true, user: user});
        }
        else{
            resolve({validPassword: false});
        }
    });
}


