'use strict'

const repository = (db) => {
  const collection = db.collection('users');

  const getUserInfo = (req,res) => {
    return new Promise((resolve, reject) => {
        db.collection("users").find({_id:req.body.payload.phone_number}).limit(1).next(function(err, result) {
            if (err) throw err;
           resolve(result);
        });
    })
  }


  const addUser = (req,res) => {
    return new Promise((resolve, reject) => {
        const hashedPassword = bcrypt.hashSync(req.body.password, 8);
        var newUser = {
            _id: req.body.payload.phone_number,
            password:hashedPassword
        };
        newUser.status = req.body.payload.status!=undefined?req.body.payload.status:"active";
        newUser.role = req.body.payload.role!= undefined?req.body.payload.role:"user";
        newUser.company = req.body.payload.company;
        console.log('Adding user: ' + JSON.stringify(newUser));
        const cursor = db.collection('users').find({ _id: req.body.payload.phone_number }).limit(1).next(function(err,userInfo){
            if(userInfo== null)
            {
                db.collection('users').save(newUser);
                resolve(newUser);
            }
        });
  });
  }

  const updateUser = (id) => {
    return new Promise((resolve, reject) => {
        db.collection('users').findOneAndUpdate({ _id: req.body.payload.disableUserId,status:"inactive" }, function(err, user) {
              if (err) throw err;

              // object of the user
              var s = user.toObject();
              return res.send(200, JSON.stringify(s));
        resolve(s);
      })
    })
  }

  const disconnect = () => {
    db.close()
  }

  return Object.create({
      getUserInfo,
      addUser,
      updateUser,
    disconnect
  })
}

const connect = (connection) => {
  return new Promise((resolve, reject) => {
    if (!connection) {
      reject(new Error('connection db not supplied!'))
    }
    resolve(repository(connection))
  })
}

module.exports = Object.assign({}, {connect})
