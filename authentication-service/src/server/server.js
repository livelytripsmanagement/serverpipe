const express = require('express')
const morgan = require('morgan')
const helmet = require('helmet')
const api = require('../api/authentication')
const bodyParser = require('body-parser')
const start = (options) => {
  return new Promise((resolve, reject) => {
    if (!options.repo) {
      reject(new Error('The server must be started with a connected repository'))
    }
    if (!options.port) {
      reject(new Error('The server must be started with an available port'))
    }

    const app = express()

      //Very important change for enabling cross domain origin ----------------Start
      app.use(function(req, res, next) {
          var oneof = false;
          /*
           if(req.originalUrl !=('/login'||'/test') && req.headers['authorization'])
           {
               res.status(401).send();
           }
           */
          if(req.headers.origin) {
              res.header('Access-Control-Allow-Origin', req.headers.origin);
              //  res.header('Access-Control-Allow-Origin', "http://api.wunderground.com");
              console.log("Origin:"+req.headers.origin);
              oneof = true;
          }
          if(req.headers['access-control-request-method']) {
              res.header('Access-Control-Allow-Methods', req.headers['access-control-request-method']);
              oneof = true;
          }
          if(req.headers['access-control-request-headers']) {
              res.header('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
              oneof = true;
          }
          if(oneof) {
              res.header('Access-Control-Max-Age', 60 * 60 * 24 * 365);
          }

          // intercept OPTIONS method
          if (oneof && req.method == 'OPTIONS') {
              res.sendStatus(200);
          }
          else {
              next();
          }
      });
//Very important change for enabling cross domain origin ----------------End
    app.use(bodyParser.json())
    app.use(morgan('dev'))
    app.use(helmet())
    app.use((err, req, res, next) => {
      reject(new Error('Something went wrong!, err:' + err))
      res.status(500).send('Something went wrong!')
    })



    api(app, options)

    const server = app.listen(options.port, () => resolve(server))
  })
}

module.exports = Object.assign({}, {start})
